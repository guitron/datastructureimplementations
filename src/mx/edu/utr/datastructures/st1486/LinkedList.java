/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructures.st1486;

import mx.edu.utr.datastructures.List;

/**
 *
 * @author Juan Pablo Alonso Guitron
 */
public class LinkedList implements List {

    //Class to create a new Node
    class Node {

        Object element;
        Node next;
        Node previous;

        //Constructor of Node
        private Node(Object element, Node next, Node previous) {
            this.element = element;
            this.next = next;
            this.previous = previous;
        }
    }

    //Variables for the cells
    int size;
    Node head;
    Node tail;

    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(int index) {
        return getNode(index).element;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {

    }
    //Set the centinels cells
    public LinkedList() {
        this.head = new Node(null, null, null);
        this.tail = new Node(null, this.head, null);
        this.head.next = this.tail;
    }

    /**
     * This methos will increase the size of the linkedlist
     *
     * @param o Specifie the conteiner for that new cell
     * @param n set the node index
     */
    public void addBefore(Object o, Node n) {

        size++;
    }
    
    /**
     *
     * Get the value in the specific index
     *
     * @param index specify a position
     */
    private Node getNode(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(Object element) {
        addBefore(element, tail);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(int index, Object element) {
        addBefore(element, getNode(index));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object set(int index, Object element) {

        Node n = getNode(index);
        Object old = n.element;
        n.element = element;
        return old;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object remove(int index) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return 0;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object o) {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return false;
    }

    
}
