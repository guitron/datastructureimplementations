package mx.edu.utr.datastructures.st1486;

import mx.edu.utr.datastructures.*;

/**
 *
 * @author Juan Pablo Alonso Guitron st1486
 */
public class ArrayList implements List {

    public Object[] elements;
    public int size;

    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    public ArrayList() {
        this(10);
    }

    

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(Object element) {
        /**
         * Inserts the specified element at the specified position in this list.
         */

        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }

    /**
     * This method will increase the capacity of the array when you want to
     * insert a new value in the list.
     *
     * @param minCapacity
     */
    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        int newCapacity;
        if (minCapacity > oldCapacity) {
            newCapacity = oldCapacity * 2;

            Object arrayList[] = new Object[newCapacity];
            for (int i = 0; i < oldCapacity; i++) {
                arrayList[i] = elements[i];
            }
            elements = arrayList;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(int index, Object element) {
        ensureCapacity(size + 1);
        for (int i = size; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        /**
         * Removes all of the elements from this list. *
         */
        for (int i = 0; i < elements.length; i++) {
            elements[i] = null;
            this.size = 0;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(int index) {

        /**
         * Returns the element at the specified position in this list.
         *
         */
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        } else {
            for (int i = 0; i <= this.size; i++) {
                if (index == i - 1) {
                    elements[i] = index;
                }
            }
            return elements[index];
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        /**
         * Returns <tt>true</tt> if this list contains no elements.
         *
         */
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object remove(int index) {
        CheckRange(index);
        Object oldElement = elements[index];

        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object set(int index, Object element) {
        /**
         * Replaces the element at the specified position in this list with the
         * specified element.
         *
         * @param index index of element to replace.
         * @param element element to be stored at the specified position.
         * @return the element previously at the specified position.
         */

        Object old = elements[index];
        elements[index] = element;
        return old;

    }

    /**
     * {@inheritDoc}
     */
    @Override 
    public int size() {
        /**
         * Returns the number of elements in this list.
         */
        return size;
    }

    /**
     * Checks the index and throws an exeption if it is incorrect
     *
     * @param index Specify a position
     */
    private void CheckRange(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        }
    }
}
